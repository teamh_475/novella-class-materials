//
//  HelloWorld2ViewController.m
//  HelloWorld
//
//  Created by Ellen Wong on 2014-06-24.
//  Copyright (c) 2014 Ellen. All rights reserved.
//

#import "HelloWorld2ViewController.h"

@interface HelloWorld2ViewController ()

@end

@implementation HelloWorld2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
