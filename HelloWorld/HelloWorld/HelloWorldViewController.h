//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by Ellen Wong on 2014-06-10.
//  Copyright (c) 2014 Ellen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController
-(IBAction)showMessage;

@end
