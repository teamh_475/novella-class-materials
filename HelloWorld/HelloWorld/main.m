//
//  main.m
//  HelloWorld
//
//  Created by Ellen Wong on 2014-06-10.
//  Copyright (c) 2014 Ellen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
