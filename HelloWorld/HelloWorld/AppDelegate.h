//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Ellen Wong on 2014-06-10.
//  Copyright (c) 2014 Ellen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
